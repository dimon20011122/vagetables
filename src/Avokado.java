public class Avokado extends Vagetables {
    String name() {
        return "Avokado";
    }

    private String shape;
    private String color = "green";
    private int weight;
    private int price;

    public Avokado(int weight, int price) {
        this.weight = weight;
        this.price = price;

    }

    double revenue() {
        return weight * price * 0.17;
    }
}
