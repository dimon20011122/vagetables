public class Cucumber extends Vagetables {
    String name() {
        return "Cucumber";
    }

    private String color = "green";
    private int weight;
    private int price;

    public Cucumber(int weight, int price) {
        this.weight = weight;
        this.price = price;
    }

    @Override
    double revenue() {
        return weight * price * 0.22;
    }
}


