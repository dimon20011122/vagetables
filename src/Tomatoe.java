public class Tomatoe extends Vagetables {
    String name() {
        return "Tomatoe";
    }

    private String color = "red";
    private int weight;
    private int price;

    public Tomatoe(int weight, int price) {
        this.weight = weight;
        this.price = price;
    }

    double revenue() {
        return weight * price * 0.3;
    }


}
